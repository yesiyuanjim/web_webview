/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ohos_adapter/ctocpp/ark_audio_capturer_options_adapter_ctocpp.h"
#include "base/ctocpp/ark_web_ctocpp_macros.h"

namespace OHOS::ArkWeb {

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetSamplingRate() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_sampling_rate, 0);

  // Execute
  return _struct->get_sampling_rate(_struct);
}

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetEncoding() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_encoding, 0);

  // Execute
  return _struct->get_encoding(_struct);
}

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetSampleFormat() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_sample_format, 0);

  // Execute
  return _struct->get_sample_format(_struct);
}

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetChannels() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_channels, 0);

  // Execute
  return _struct->get_channels(_struct);
}

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetSourceType() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_source_type, 0);

  // Execute
  return _struct->get_source_type(_struct);
}

ARK_WEB_NO_SANITIZE
int32_t ArkAudioCapturerOptionsAdapterCToCpp::GetCapturerFlags() {
  ARK_WEB_CTOCPP_DV_LOG("capi struct is %{public}ld", (long)this);

  ark_audio_capturer_options_adapter_t *_struct = GetStruct();
  ARK_WEB_CTOCPP_CHECK_PARAM(_struct, 0);

  ARK_WEB_CTOCPP_CHECK_FUNC_MEMBER(_struct, get_capturer_flags, 0);

  // Execute
  return _struct->get_capturer_flags(_struct);
}

ArkAudioCapturerOptionsAdapterCToCpp::ArkAudioCapturerOptionsAdapterCToCpp() {
}

ArkAudioCapturerOptionsAdapterCToCpp::~ArkAudioCapturerOptionsAdapterCToCpp() {
}

template <>
ArkWebBridgeType ArkWebCToCppRefCounted<
    ArkAudioCapturerOptionsAdapterCToCpp, ArkAudioCapturerOptionsAdapter,
    ark_audio_capturer_options_adapter_t>::kBridgeType =
    ARK_AUDIO_CAPTURER_OPTIONS_ADAPTER;

} // namespace OHOS::ArkWeb
